<?php

use App\Mail\TestAmazonSes;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (env('ENFORCE_SSL', false) && App::environment('production')) {
    URL::forceScheme('https');
}

Route::get('test', function () {
    Mail::to(env('MAIL_TO_ADDRESS'))->send(new TestAmazonSes('Mail sent using AWS SES'));
    return view('welcome');
});
